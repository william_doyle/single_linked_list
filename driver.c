#include "linkedList.h"


int main (void){
	
	struct llist * mylist = make_llist(__compare_int);
	int * n1 = malloc(sizeof(int));
	*n1 = 2; // heap
	int n2 = 5; // stack
	mylist->push( mylist, (void*)n1);
	mylist->push( mylist, (void*)&n2);
	mylist->print(mylist);

	printf("Popped Value Is %p\n", mylist->pop(mylist)); 
	
	
	destroy_llist(mylist);
	free(n1);
	return EXIT_SUCCESS;
}
