#include "linkedList.h"

// William Doyle
// Febuary 14th 2020

int __compare_int(void * vnum1, void * vnum2){
	if (*(int*)vnum1 == *(int*)vnum2){
		return 1;
	}return 0;
}


// constructors
struct llist * make_llist (int(*__compare_func)(void*,void*)){
	struct llist * self = malloc(sizeof(struct llist));
	if (!self){
		fprintf(stderr, "Malloc failed in function %s\n", __func__);
		abort();
	}
	self->top = NULL;
	self->push = __push;
	self->pop = __pop;
	self->print = __print;
//	self->pop_last = __pop_last;
//	self->pop_match = __pop_match;
	self->compare = __compare_func;
	return self;
}

struct lnode * make_lnode(void * __data){
	struct lnode * self = malloc(sizeof(struct lnode));
	if (!self){
		fprintf(stderr, "Malloc failed in function %s\n", __func__);
		abort();
	}
	self->next = NULL;
	self->data = __data;
}

void destroy_lnode(struct lnode * self){
	if (self){
		free(self);
	} else {
		fprintf(stderr, "Nothing to destroy in function %s\n", __func__);
	}
}

void destroy_llist(struct llist * self){
	if (!self){
		fprintf(stderr, "Nothing to destroy in function %s\n", __func__);
		return;
	}
	// destroy lnodes
	struct lnode * temp = self->top;
	struct lnode * _next = NULL;
	while (temp != NULL){
		_next = temp->next;
		free(temp);
		temp = _next;
	}
	// destroy self
	free(self);
}

void __push(struct llist * self, void * __data){
	if (self->top == NULL){
		self->top = make_lnode(__data);
		return;
	}
	int placed = 0;
	struct lnode * curpos = self->top;
	while (placed == 0){
		if (curpos->next == NULL){
			curpos->next = make_lnode(__data);
			placed = 1;
			continue;
		}
		curpos = curpos->next;
	}
}

void* __pop (struct llist *self){
	void * rval = self->top->data; // return value
	struct lnode * _top = self->top;
	self->top = self->top->next;
	if (_top != NULL){
		free(_top);
	} else {
		fprintf(stderr, "Could not free \"top\" in function %s\n", __func__);
	}
	return rval;
}

void __print(struct llist * self ){
	for (struct lnode * i = self->top; i != NULL; i = i->next){
		printf("%p\t%d\n", i->data, *(int*)i->data);
	}
	printf("\n");	
}








