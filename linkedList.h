#pragma once
#include <stdio.h>
#include <stdlib.h>

struct lnode{
	void * data;
	struct lnode * next;
};

struct llist{
	struct lnode * top;
	void (*push)( struct llist *, void *);
	void* (*pop)(struct llist *);
	void* (*pop_last)(struct llist * );
	void* (*pop_match)(struct llist * , void * data);
	int (*compare)(void *, void *);
	void (*print)(struct llist *);
};

// constructors
struct llist * make_llist (int(*)(void*,void*));
struct lnode * make_lnode(void*);
// destructors
void destroy_llist(struct llist*);
void destroy_lnode(struct lnode*);

void __push(struct llist *, void * );
	
void* __pop (struct llist *);
void* __pop_last(struct llist * );
void* __pop_match(struct llist * , void * data);
int __compare_int(void *, void *);
int __compare_alphabet(void *, void *);
void __print(struct llist *);
